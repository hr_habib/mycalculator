package com.iamhabib;

import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;

import com.iamhabib.utils.CalculateFactorial;
import com.iamhabib.utils.ExtendedDoubleEvaluator;

public class ScientificCal extends AppCompatActivity {

    private EditText et_store_expresion, et_display;
    private int count = 0;
    private String expression = "";
    private String text = "";
    private Double result = 0.0;
    private Button mode, toggle, square, xpowy, log, sin, cos, tan, sqrt, fact;
    private int toggleMode = 1;
    private int angleMode = 1;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_scientific_cal);
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);

        et_store_expresion = (EditText) findViewById(R.id.editText);
        et_display = (EditText) findViewById(R.id.editText2);
        mode = (Button) findViewById(R.id.mode);
        toggle = (Button) findViewById(R.id.toggle);
        square = (Button) findViewById(R.id.square);
        xpowy = (Button) findViewById(R.id.xpowy);
        log = (Button) findViewById(R.id.log);
        sin = (Button) findViewById(R.id.sin);
        cos = (Button) findViewById(R.id.cos);
        tan = (Button) findViewById(R.id.tan);
        sqrt = (Button) findViewById(R.id.sqrt);
        fact = (Button) findViewById(R.id.factorial);
        et_display.setText("0");
        mode.setTag(1);
        toggle.setTag(1);
    }

    public void onClick(View v) {
        toggleMode = (int) toggle.getTag();
        angleMode = ((int) mode.getTag());
        switch (v.getId()) {
            case R.id.toggle:
                if (toggleMode == 1) {
                    toggle.setTag(2);
                    square.setText(R.string.cube);
                    xpowy.setText(R.string.tenpow);
                    log.setText(R.string.naturalLog);
                    sin.setText(R.string.sininv);
                    cos.setText(R.string.cosinv);
                    tan.setText(R.string.taninv);
                    sqrt.setText(R.string.cuberoot);
                    fact.setText(R.string.Mod);
                } else if (toggleMode == 2) {
                    toggle.setTag(3);
                    square.setText(R.string.square);
                    xpowy.setText(R.string.epown);
                    log.setText(R.string.log);
                    sin.setText(R.string.hyperbolicSine);
                    cos.setText(R.string.hyperbolicCosine);
                    tan.setText(R.string.hyperbolicTan);
                    sqrt.setText(R.string.inverse);
                    fact.setText(R.string.factorial);
                } else if (toggleMode == 3) {
                    toggle.setTag(1);
                    sin.setText(R.string.sin);
                    cos.setText(R.string.cos);
                    tan.setText(R.string.tan);
                    sqrt.setText(R.string.sqrt);
                    xpowy.setText(R.string.xpown);
                }
                break;
            case R.id.mode:
                if (angleMode == 1) {
                    mode.setTag(2);
                    mode.setText(R.string.mode2);
                } else {
                    mode.setTag(1);
                    mode.setText(R.string.mode1);
                }
                break;

            case R.id.num0:
                et_display.setText(et_display.getText() + "0");
                break;
            case R.id.num1:
                et_display.setText(et_display.getText() + "1");
                break;
            case R.id.num2:
                et_display.setText(et_display.getText() + "2");
                break;
            case R.id.num3:
                et_display.setText(et_display.getText() + "3");
                break;
            case R.id.num4:
                et_display.setText(et_display.getText() + "4");
                break;

            case R.id.num5:
                et_display.setText(et_display.getText() + "5");
                break;
            case R.id.num6:
                et_display.setText(et_display.getText() + "6");
                break;
            case R.id.num7:
                et_display.setText(et_display.getText() + "7");
                break;
            case R.id.num8:
                et_display.setText(et_display.getText() + "8");
                break;

            case R.id.num9:
                et_display.setText(et_display.getText() + "9");
                break;
            case R.id.pi:
                et_display.setText(et_display.getText() + "pi");
                break;
            case R.id.dot:
                if (count == 0 && et_display.length() != 0) {
                    et_display.setText(et_display.getText() + ".");
                    count++;
                }
                break;
            case R.id.clear:
                et_store_expresion.setText("");
                et_display.setText("");
                count = 0;
                expression = "";
                break;
            case R.id.backSpace:
                text = et_display.getText().toString();
                if (text.length() > 0) {
                    if (text.endsWith(".")) {
                        count = 0;
                    }
                    String newText = text.substring(0, text.length() - 1);
                    if (text.endsWith(")")) {
                        char[] a = text.toCharArray();
                        int pos = a.length - 2;
                        int counter = 1;
                        for (int i = a.length - 2; i >= 0; i--) {
                            if (a[i] == ')') {
                                counter++;
                            } else if (a[i] == '(') {
                                counter--;
                            } else if (a[i] == '.') {
                                count = 0;
                            }
                            if (counter == 0) {
                                pos = i;
                                break;
                            }
                        }
                        newText = text.substring(0, pos);
                    }
                    if (newText.equals("-") || newText.endsWith("sqrt") || newText.endsWith("log") || newText.endsWith("ln")
                            || newText.endsWith("sin") || newText.endsWith("asin") || newText.endsWith("asind") || newText.endsWith("sinh")
                            || newText.endsWith("cos") || newText.endsWith("acos") || newText.endsWith("acosd") || newText.endsWith("cosh")
                            || newText.endsWith("tan") || newText.endsWith("atan") || newText.endsWith("atand") || newText.endsWith("tanh")
                            || newText.endsWith("cbrt")) {
                        newText = "";
                    } else if (newText.endsWith("^") || newText.endsWith("/"))
                        newText = newText.substring(0, newText.length() - 1);
                    else if (newText.endsWith("pi") || newText.endsWith("e^"))
                        newText = newText.substring(0, newText.length() - 2);
                    et_display.setText(newText);
                }
                break;
            case R.id.plus:
                operationClicked("+");
                break;
            case R.id.minus:
                operationClicked("-");
                break;
            case R.id.divide:
                operationClicked("/");
                break;
            case R.id.multiply:
                operationClicked("*");
                break;
            case R.id.sqrt:
                if (et_display.length() != 0) {
                    text = et_display.getText().toString();
                    toggleMode = (int) toggle.getTag();
                    if (toggleMode == 1)
                        et_display.setText("sqrt(" + text + ")");
                    else if (toggleMode == 2)
                        et_display.setText("cbrt(" + text + ")");
                    else
                        et_display.setText("1/(" + text + ")");
                }
                break;
            case R.id.square:
                if (et_display.length() != 0) {
                    text = et_display.getText().toString();
                    if (toggleMode == 2)
                        et_display.setText("(" + text + ")^3");
                    else
                        et_display.setText("(" + text + ")^2");
                }
                break;
            case R.id.xpowy:
                if (et_display.length() != 0) {
                    text = et_display.getText().toString();
                    if (toggleMode == 1)
                        et_display.setText("(" + text + ")^");
                    else if (toggleMode == 2)
                        et_display.setText("10^(" + text + ")");
                    else
                        et_display.setText("e^(" + text + ")");
                }
                break;
            case R.id.log:
                if (et_display.length() != 0) {
                    text = et_display.getText().toString();
                    if (toggleMode == 2)
                        et_display.setText("ln(" + text + ")");
                    else
                        et_display.setText("log(" + text + ")");
                }
                break;
            case R.id.factorial:
                if (et_display.length() != 0) {
                    text = et_display.getText().toString();
                    if (toggleMode == 2) {
                        et_store_expresion.setText("(" + text + ")%");
                        et_display.setText("");
                    } else {
                        String res = "";
                        try {
                            CalculateFactorial cf = new CalculateFactorial();
                            int[] arr = cf.factorial((int) Double.parseDouble(String.valueOf(new ExtendedDoubleEvaluator().evaluate(text))));
                            int res_size = cf.getRes();
                            if (res_size > 20) {
                                for (int i = res_size - 1; i >= res_size - 20; i--) {
                                    if (i == res_size - 2)
                                        res += ".";
                                    res += arr[i];
                                }
                                res += "E" + (res_size - 1);
                            } else {
                                for (int i = res_size - 1; i >= 0; i--) {
                                    res += arr[i];
                                }
                            }
                            et_display.setText(res);
                        } catch (Exception e) {
                            if (e.toString().contains("ArrayIndexOutOfBoundsException")) {
                                et_display.setText("Result too big!");
                            } else
                                et_display.setText("Invalid!!");
                            e.printStackTrace();
                        }
                    }
                }
                break;

            case R.id.sin:
                if (et_display.length() != 0) {
                    text = et_display.getText().toString();
                    if (angleMode == 1) {
                        double angle = Math.toRadians(new ExtendedDoubleEvaluator().evaluate(text));
                        if (toggleMode == 1)
                            et_display.setText("sin(" + angle + ")");
                        else if (toggleMode == 2)
                            et_display.setText("asind(" + text + ")");
                        else
                            et_display.setText("sinh(" + text + ")");
                    } else {
                        if (toggleMode == 1)
                            et_display.setText("sin(" + text + ")");
                        else if (toggleMode == 2)
                            et_display.setText("asin(" + text + ")");
                        else
                            et_display.setText("sinh(" + text + ")");
                    }
                }
                break;
            case R.id.cos:
                if (et_display.length() != 0) {
                    text = et_display.getText().toString();
                    if (angleMode == 1) {
                        double angle = Math.toRadians(new ExtendedDoubleEvaluator().evaluate(text));
                        if (toggleMode == 1)
                            et_display.setText("cos(" + angle + ")");
                        else if (toggleMode == 2)
                            et_display.setText("acosd(" + text + ")");
                        else
                            et_display.setText("cosh(" + text + ")");
                    } else {
                        if (toggleMode == 1)
                            et_display.setText("cos(" + text + ")");
                        else if (toggleMode == 2)
                            et_display.setText("acos(" + text + ")");
                        else
                            et_display.setText("cosh(" + text + ")");
                    }
                }
                break;
            case R.id.tan:
                if (et_display.length() != 0) {
                    text = et_display.getText().toString();
                    if (angleMode == 1) {
                        double angle = Math.toRadians(new ExtendedDoubleEvaluator().evaluate(text));
                        if (toggleMode == 1)
                            et_display.setText("tan(" + angle + ")");
                        else if (toggleMode == 2)
                            et_display.setText("atand(" + text + ")");
                        else
                            et_display.setText("tanh(" + text + ")");
                    } else {
                        if (toggleMode == 1)
                            et_display.setText("tan(" + text + ")");
                        else if (toggleMode == 2)
                            et_display.setText("atan(" + text + ")");
                        else
                            et_display.setText("tanh(" + text + ")");
                    }
                }
                break;
            case R.id.posneg:
                if (et_display.length() != 0) {
                    String s = et_display.getText().toString();
                    char arr[] = s.toCharArray();
                    if (arr[0] == '-')
                        et_display.setText(s.substring(1, s.length()));
                    else
                        et_display.setText("-" + s);
                }
                break;
            case R.id.equal:
                if (et_display.length() != 0) {
                    text = et_display.getText().toString();
                    expression = et_store_expresion.getText().toString() + text;
                }
                et_store_expresion.setText("");
                if (expression.length() == 0)
                    expression = "0.0";
                try {
                    result = new ExtendedDoubleEvaluator().evaluate(expression);
                    if (String.valueOf(result).equals("6.123233995736766E-17")) {
                        result = 0.0;
                        et_display.setText(result + "");
                    } else if (String.valueOf(result).equals("1.633123935319537E16"))
                        et_display.setText("infinity");
                    else
                        et_display.setText(result + "");
                } catch (Exception e) {
                    et_display.setText("Invalid Expression");
                    et_store_expresion.setText("");
                    expression = "";
                    e.printStackTrace();
                }
                break;
            case R.id.openBracket:
                et_store_expresion.setText(et_store_expresion.getText() + "(");
                break;
            case R.id.closeBracket:
                if (et_display.length() != 0)
                    et_store_expresion.setText(et_store_expresion.getText() + et_display.getText().toString() + ")");
                else
                    et_store_expresion.setText(et_store_expresion.getText() + ")");
                break;
        }
    }

    private void operationClicked(String op) {
        if (et_display.length() != 0) {
            String text = et_display.getText().toString();
            et_store_expresion.setText(et_store_expresion.getText() + text + op);
            et_display.setText("");
            count = 0;
        } else {
            String text = et_store_expresion.getText().toString();
            if (text.length() > 0) {
                String newText = text.substring(0, text.length() - 1) + op;
                et_store_expresion.setText(newText);
            }
        }
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        if (item.getItemId() == android.R.id.home) {
            onBackPressed();
        }
        return super.onOptionsItemSelected(item);
    }
}