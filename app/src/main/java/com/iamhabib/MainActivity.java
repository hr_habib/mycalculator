package com.iamhabib;

import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.View;

public class MainActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        getSupportActionBar().setLogo(R.mipmap.ic_launcher);
    }

    public void onClick(View view) {
        Intent i;
        if (view.getId() == R.id.button1) {
            i = new Intent(this, StandardCal.class);
            startActivity(i);
        } else if (view.getId() == R.id.button) {
            i = new Intent(this, ScientificCal.class);
            startActivity(i);
        } else if (view.getId() == R.id.button2) {
            i = new Intent(this, UnitTemperature.class);
            startActivity(i);
        } else if (view.getId() == R.id.button3) {
            i = new Intent(this, UnitLength.class);
            startActivity(i);
        }
    }
}
