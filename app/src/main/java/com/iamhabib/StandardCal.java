package com.iamhabib;

import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.MenuItem;
import android.view.View;
import android.widget.EditText;

import com.fathzer.soft.javaluator.DoubleEvaluator;
import com.iamhabib.utils.ExtendedDoubleEvaluator;

public class StandardCal extends AppCompatActivity {

    private EditText et_store_expresion, et_display;
    private int count = 0;
    private String expression = "";
    private String text = "";
    private Double result = 0.0;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_standard_cal);
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        et_store_expresion = (EditText) findViewById(R.id.editText1);
        et_display = (EditText) findViewById(R.id.editText2);
        et_display.setText("0");
    }

    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.num0:
                et_display.setText(et_display.getText() + "0");
                break;
            case R.id.num1:
                et_display.setText(et_display.getText() + "1");
                break;
            case R.id.num2:
                et_display.setText(et_display.getText() + "2");
                break;
            case R.id.num3:
                et_display.setText(et_display.getText() + "3");
                break;
            case R.id.num4:
                et_display.setText(et_display.getText() + "4");
                break;
            case R.id.num5:
                et_display.setText(et_display.getText() + "5");
                break;
            case R.id.num6:
                et_display.setText(et_display.getText() + "6");
                break;
            case R.id.num7:
                et_display.setText(et_display.getText() + "7");
                break;
            case R.id.num8:
                et_display.setText(et_display.getText() + "8");
                break;
            case R.id.num9:
                et_display.setText(et_display.getText() + "9");
                break;
            case R.id.dot:
                if (count == 0 && et_display.length() != 0) {
                    et_display.setText(et_display.getText() + ".");
                    count++;
                }
                break;
            case R.id.clear:
                et_store_expresion.setText("");
                et_display.setText("");
                count = 0;
                expression = "";
                break;
            case R.id.backSpace:
                text = et_display.getText().toString();
                if (text.length() > 0) {
                    if (text.endsWith(".")) {
                        count = 0;
                    }
                    String newText = text.substring(0, text.length() - 1);
                    if (text.endsWith(")")) {
                        char[] a = text.toCharArray();
                        int pos = a.length - 2;
                        int counter = 1;
                        for (int i = a.length - 2; i >= 0; i--) {
                            if (a[i] == ')') {
                                counter++;
                            } else if (a[i] == '(') {
                                counter--;
                            } else if (a[i] == '.') {
                                count = 0;
                            }
                            if (counter == 0) {
                                pos = i;
                                break;
                            }
                        }
                        newText = text.substring(0, pos);
                    }
                    if (newText.equals("-") || newText.endsWith("sqrt")) {
                        newText = "";
                    } else if (newText.endsWith("^"))
                        newText = newText.substring(0, newText.length() - 1);
                    et_display.setText(newText);
                }
                break;
            case R.id.plus:
                operationClicked("+");
                break;
            case R.id.minus:
                operationClicked("-");
                break;
            case R.id.divide:
                operationClicked("/");
                break;
            case R.id.multiply:
                operationClicked("*");
                break;
            case R.id.posneg:
                if (et_display.length() != 0) {
                    String s = et_display.getText().toString();
                    char arr[] = s.toCharArray();
                    if (arr[0] == '-')
                        et_display.setText(s.substring(1, s.length()));
                    else
                        et_display.setText("-" + s);
                }
                break;
            case R.id.equal:
                if (et_display.length() != 0) {
                    text = et_display.getText().toString();
                    expression = et_store_expresion.getText().toString() + text;
                }
                et_store_expresion.setText("");
                if (expression.length() == 0)
                    expression = "0.0";
                DoubleEvaluator evaluator = new DoubleEvaluator();
                try {
                    result = new ExtendedDoubleEvaluator().evaluate(expression);
                    if (!expression.equals("0.0"))
                        et_display.setText(result + "");
                } catch (Exception e) {
                    et_display.setText("Invalid Expression");
                    et_store_expresion.setText("");
                    expression = "";
                    e.printStackTrace();
                }
                break;
        }
    }

    private void operationClicked(String op) {
        if (et_display.length() != 0) {
            String text = et_display.getText().toString();
            et_store_expresion.setText(et_store_expresion.getText() + text + op);
            et_display.setText("");
            count = 0;
        } else {
            String text = et_store_expresion.getText().toString();
            if (text.length() > 0) {
                String newText = text.substring(0, text.length() - 1) + op;
                et_store_expresion.setText(newText);
            }
        }
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        if (item.getItemId() == android.R.id.home) {
            onBackPressed();
        }
        return super.onOptionsItemSelected(item);
    }
}
