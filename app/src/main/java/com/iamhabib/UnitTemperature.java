package com.iamhabib;

import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.MenuItem;
import android.view.View;
import android.widget.EditText;
import android.widget.Spinner;

import com.iamhabib.utils.ConvertingUnits;

public class UnitTemperature extends AppCompatActivity {

    private EditText et_store_expresion, et_display;
    private Spinner s1, s2;
    private int count1 = 0;
    private ConvertingUnits.Temperature ca;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_unit_temperature);
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        et_store_expresion = (EditText) findViewById(R.id.item1);
        et_display = (EditText) findViewById(R.id.item2);
        s1 = (Spinner) findViewById(R.id.spinner1);
        s2 = (Spinner) findViewById(R.id.spinner2);
        ca = new ConvertingUnits.Temperature();
    }

    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.num0:
                et_store_expresion.setText(et_store_expresion.getText() + "0");
                break;
            case R.id.num1:
                et_store_expresion.setText(et_store_expresion.getText() + "1");
                break;
            case R.id.num2:
                et_store_expresion.setText(et_store_expresion.getText() + "2");
                break;
            case R.id.num3:
                et_store_expresion.setText(et_store_expresion.getText() + "3");
                break;
            case R.id.num4:
                et_store_expresion.setText(et_store_expresion.getText() + "4");
                break;
            case R.id.num5:
                et_store_expresion.setText(et_store_expresion.getText() + "5");
                break;
            case R.id.num6:
                et_store_expresion.setText(et_store_expresion.getText() + "6");
                break;
            case R.id.num7:
                et_store_expresion.setText(et_store_expresion.getText() + "7");
                break;
            case R.id.num8:
                et_store_expresion.setText(et_store_expresion.getText() + "8");
                break;
            case R.id.num9:
                et_store_expresion.setText(et_store_expresion.getText() + "9");
                break;
            case R.id.dot:
                if (count1 == 0) {
                    et_store_expresion.setText(et_store_expresion.getText() + ".");
                    count1++;
                }
                break;
            case R.id.clear:
                et_store_expresion.setText("");
                et_display.setText("");
                count1 = 0;
                break;
            case R.id.backSpace:
                if (et_store_expresion.length() != 0) {
                    String text = et_store_expresion.getText().toString();
                    if (text.endsWith("."))
                        count1 = 0;
                    String newText = text.substring(0, text.length() - 1);
                    et_store_expresion.setText(newText);
                }
                break;
            case R.id.equal:
                try {
                    int item1 = s1.getSelectedItemPosition();
                    int item2 = s2.getSelectedItemPosition();
                    double value1 = Double.parseDouble(et_store_expresion.getText().toString());
                    double result = evaluate(item1, item2, value1);
                    et_display.setText(result + "");
                } catch (Exception e) {
                    et_display.setText("0.0");
                }
                break;
        }
    }

    public double evaluate(int item1, int item2, double value) {
        double temp = 0.0;
        if (item1 == item2)
            return value;
        else {
            switch (item1) {
                case 0:
                    temp = ca.CelsiTokelvin(value);
                    break;
                case 1:
                    temp = ca.FerToKelvin(value);
                    break;
                case 2:
                    temp = value;
                    break;
            }

            switch (item2) {
                case 0:
                    temp = ca.KelvinToCelsi(temp);
                    break;
                case 1:
                    temp = ca.KelvinToFer(temp);
                    break;
            }
            return temp;
        }
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        if (item.getItemId() == android.R.id.home) {
            onBackPressed();
        }
        return super.onOptionsItemSelected(item);
    }
}
